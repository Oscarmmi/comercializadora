<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subseries extends Model
{
    protected $primaryKey = 'idsubserie';
}
