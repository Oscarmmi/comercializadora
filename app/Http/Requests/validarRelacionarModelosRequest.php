<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class validarRelacionarModelosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'inicio' => 'required|numeric',
            'final' => 'required|numeric',
            'idsubserie' => 'required|numeric'
       ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'idsubserie.numeric' => '- La subserie ingresada no es valida',
            'idsubserie.required' => '- Debe seleccionar una subserie',
            'inicio.required'  => '- Debe ingresar un año inicial', 
            'inicio.numeric'  => '- El año inicial no es valido', 
            'fin.required'  => '- Debe ingresar un año final', 
            'fin.numeric'  => '- El año final no es valido' 
        ];
    }
}
