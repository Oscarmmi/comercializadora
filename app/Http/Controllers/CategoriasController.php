<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\validarCategoriaRequest;

use App\Categorias;

class CategoriasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            $id=$request->input('id');
            $datos=array();
            if(!$id){
                $aWhere=array();
                $nombre=$request->input('nombre');
                if($nombre){
                    $aWhere[]=array('categorias.nombre', 'LIKE', '%'.strtolower($nombre).'%');
                }
                $observacion=$request->input('observacion');
                if($observacion){
                    $aWhere[]=array('categorias.observacion', 'LIKE', '%'.$observacion.'%');
                }
                $datos=DB::table('categorias')
                ->select('categorias.idcategoria as id', 'categorias.nombre', 'categorias.observacion')
                ->where($aWhere) 
                ->get();
            }else{
                $datos=DB::table('categorias')
                ->select('categorias.*')
                ->where('idcategoria', '=', $id)
                ->get();       
            }
            return $datos;            
        }else{
            return view('home');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(validarCategoriaRequest $request)
    {
        $validatedData = $request->validated();
        $categoria = new Categorias();
        return $this->guardarRegistro($validatedData, $categoria); 
    }

    public function validarNombreCategoriaUnico($aDatos){
        $aWhere=array();
        $aWhere[]=array('nombre', '=', $aDatos['nombre']);
        if(isset($aDatos['idcategoria'])){
            $aWhere[]=array('idcategoria', '!=', $aDatos['idcategoria']);
        }
        $datos=DB::table('categorias')
                ->select('categorias.*')
                ->where($aWhere)
                ->get();
        if(count($datos)){
            throw ValidationException::withMessages(['nombre' => '- El nombre de la categoria ya se encuentra registrado']);
        }
    }

    public function guardarRegistro($aDatos, $categoria){
        $this->validarNombreCategoriaUnico($aDatos);
        $categoria->nombre = $aDatos['nombre'];
        $observacion="";
        if(isset($aDatos['observacion'])){
            $observacion=$aDatos['observacion'];
        }
        $idusuario= auth()->user()->id;
        $categoria->user_id=$idusuario;
        $categoria->observacion = $observacion;
        $categoria->save();
        return $categoria;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(validarCategoriaRequest $request, $id)
    {
        $validatedData = $request->validated();
        $categoria = Categorias::find($id);        
        return $this->guardarRegistro($validatedData, $categoria);    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $categoria = Categorias::find($id);
        $categoria->delete();
    }
}
