<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use App\Http\Requests\validarSubserieRequest;
use App\Http\Requests\validarRelacionarModelosRequest;

use App\Subseries;

class SubseriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            $id=$request->input('id');
            $datos=array();
            if(!$id){
                $aWhere=array();
                $nombre=$request->input('nombre');
                if($nombre){
                    $aWhere[]=array('subseries.nombre', 'LIKE', '%'.strtolower($nombre).'%');
                }
                $idmarca=$request->input('idmarca');
                if($idmarca){
                    $aWhere[]=array('series.idmarca', '=', $idmarca);
                }
                $idserie=$request->input('idserie');
                if($idserie){
                    $aWhere[]=array('series.idserie', '=', $idserie);
                }
                $datos=DB::table('subseries')
                ->join('series', 'series.idserie', '=', 'subseries.idserie')
                ->join('marcas', 'marcas.idmarca', '=', 'series.idmarca')
                ->select('subseries.idsubserie as id', 'subseries.nombre', 'series.nombre as serie', 'marcas.nombre as marca', 'subseries.observacion')
                ->where($aWhere) 
                ->get();
            }else{
                $datos=DB::table('subseries')
                ->join('series', 'series.idserie', '=', 'subseries.idserie')
                ->select('subseries.*', 'series.idmarca')
                ->where('subseries.idsubserie', '=', $id)
                ->get();       
            }
            return $datos;            
        }else{
            return view('home');
        }
    }

    public function buscarAnnos(){
        return DB::table('annos')
            ->select('annos.idanno as value', 'annos.anno as text')
            ->get();
    }

    public function buscarSeries(Request $request){
        $idmarca=$request->input('idmarca');
        if(!isset($idmarca)){
            throw ValidationException::withMessages(['marca' => '- Debe seleccionar una marca']);
        }
        $aWhere=array();
        $aWhere[]=array('series.idmarca','=', $idmarca);
        return DB::table('series')
            ->select('series.idserie as value', 'series.nombre as text')
            ->where($aWhere)
            ->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(validarSubserieRequest $request)
    {
        $validatedData = $request->validated();
        $subserie = new Subseries();        
        return $this->guardarRegistro($validatedData, $subserie);  
    }

    public function validarNombreSubserieUnico($aDatos){
        $aWhere=array();
        if(!isset($aDatos['nombre'])){
            throw ValidationException::withMessages(['nombre' => '- El nombre de la subserie no puede estar vacio']);
        }
        $aDatos['nombre']=strtolower(trim($aDatos['nombre']));
        $aWhere[]=array('nombre', '=', $aDatos['nombre']);
        $aWhere[]=array('idserie', '=', $aDatos['idserie']);
        if(isset($aDatos['idsubserie'])){
            $aWhere[]=array('idsubserie', '!=', $aDatos['idsubserie']);
        }
        $datos=DB::table('subseries')
                ->select('subseries.*')
                ->where($aWhere)
                ->get();
        if(count($datos)){
            throw ValidationException::withMessages(['nombre' => '- El nombre de la subserie ya se encuentra registrado']);
        }
    }

    public function relacionarmodelos(validarRelacionarModelosRequest $request){
        $validatedData = $request->validated();
        var_dump($validatedData);        
        //throw ValidationException::withMessages(['nombre' => '- El nombre de la subserie no puede estar vacio']);
    }

    public function guardarRegistro($aDatos, $subserie){
        $this->validarNombreSubserieUnico($aDatos);
        $subserie->nombre = $aDatos['nombre'];
        $observacion="";
        if(isset($aDatos['observacion'])){
            $observacion=$aDatos['observacion'];
        }
        $idusuario= auth()->user()->id;
        $subserie->user_id=$idusuario;
        $subserie->observacion = $observacion;
        $subserie->idserie = $aDatos['idserie'];
        $subserie->save();
        return $subserie;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(validarSubserieRequest $request, $id)
    {
        $validatedData = $request->validated();
        $subserie = Subseries::find($id);        
        return $this->guardarRegistro($validatedData, $subserie);  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subserie = Subseries::find($id);
        $subserie->delete();       
    }
}
