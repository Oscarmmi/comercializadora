<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/marcas/paises', 'MarcasController@buscarPaises')->middleware('auth');
Route::resource('/marcas', 'MarcasController')->middleware('auth');

Route::get('/series/marcas', 'SeriesController@buscarMarcas')->middleware('auth');
Route::resource('/series', 'SeriesController')->middleware('auth');

Route::get('/subseries/series', 'SubseriesController@buscarSeries')->middleware('auth');
Route::get('/subseries/annos', 'SubseriesController@buscarAnnos')->middleware('auth');
Route::post('/relacionarmodelos', 'SubseriesController@relacionarmodelos')->middleware('auth');
Route::resource('/subseries', 'SubseriesController')->middleware('auth');

Route::resource('/categorias', 'CategoriasController')->middleware('auth');

Route::get('/subcategorias/categorias', 'SubcategoriasController@buscarCategorias')->middleware('auth');
Route::resource('/subcategorias', 'SubcategoriasController')->middleware('auth');
