<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SeriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if($request->ajax()){
            $id=$request->input('id');
            $datos=array();
            if(!$id){
                $aWhere=array();
                $nombre=$request->input('nombre');
                if($nombre){
                    $aWhere[]=array('series.nombre', 'LIKE', '%'.strtolower($nombre).'%');
                }
                $idpais=$request->input('idmarca');
                if($idpais){
                    $aWhere[]=array('series.idmarca', '=', $idmarca);
                }
                $datos=DB::table('marcas')
                ->join('paises', 'marcas.idpais', '=', 'paises.idpais')
                ->select('marcas.idmarca as id', 'marcas.nombre', 'paises.nombre as pais', 'marcas.observacion')
                ->where($aWhere) 
                ->get();
            }else{
                $datos=DB::table('marcas')
                ->select('marcas.*')
                ->where('idmarca', '=', $id)
                ->get();       
            }
            return $datos;            
        }else{
            return view('home');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
