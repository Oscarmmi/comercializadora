<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Series;

class SeriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            $id=$request->input('id');
            $datos=array();
            if(!$id){
                $aWhere=array();
                $nombre=$request->input('nombre');
                if($nombre){
                    $aWhere[]=array('series.nombre', 'LIKE', '%'.strtolower($nombre).'%');
                }
                $idmarca=$request->input('idmarca');
                if($idmarca){
                    $aWhere[]=array('series.idmarca', '=', $idmarca);
                }
                $datos=DB::table('series')
                ->join('marcas', 'marcas.idmarca', '=', 'series.idmarca')
                ->select('series.idserie as id', 'series.nombre', 'marcas.nombre as marca', 'series.observacion')
                ->where($aWhere) 
                ->get();
            }else{
                $datos=DB::table('series')
                ->select('series.*')
                ->where('idserie', '=', $id)
                ->get();       
            }
            return $datos;            
        }else{
            return view('home');
        }
    }

    public function buscarMarcas(){
        return DB::table('marcas')
            ->select('marcas.idmarca as value', 'marcas.nombre as text')
            ->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(validarMarcaRequest $request)
    {
        $validatedData = $request->validated();
        $serie = new Series();        
        return $this->guardarRegistro($validatedData, $serie);    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
