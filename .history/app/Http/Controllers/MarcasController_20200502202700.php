<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\validarMarcaRequest;

use App\Marcas;

class MarcasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            $id=$request->input('id');
            $datos=array();
            if(!$id){
                $aWhere=array();
                $nombre=$request->input('nombre');
                if($nombre){
                    $aWhere[]=array('marcas.nombre', 'LIKE', '%'.strtolower($nombre).'%');
                }
                $idpais=$request->input('idpais');
                if($idpais){
                    $aWhere[]=array('marcas.idpais', '=', $idpais);
                }
                $datos=DB::table('marcas')
                ->join('paises', 'marcas.idpais', '=', 'paises.idpais')
                ->select('marcas.idmarca as id', 'marcas.nombre', 'paises.nombre as pais', 'marcas.observacion')
                ->where($aWhere) 
                ->get();
            }else{
                $datos=DB::table('marcas')
                ->select('marcas.*')
                ->where('idmarca', '=', $id)
                ->get();       
            }
            return $datos;            
        }else{
            return view('home');
        }
    }

    public function buscarPaises(){
        return DB::table('paises')
            ->select('paises.idpais as value', 'paises.nombre as text')
            ->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(validarMarcaRequest $request)
    {
        $marca = new Marcas();
        $validatedData = $request->validated();
        return $this->guardarRegistro($validatedData, $marca);        
    }

    public function validarNombreMarcaUnico($aDatos){
        if()
    }

    public function guardarRegistro($aDatos, $marca){
        $this->validarNombreMarcaUnico($aDatos);
        $marca->nombre = $aDatos['nombre'];
        $observacion="";
        if($aDatos['observacion']){
            $observacion=$aDatos['observacion'];
        }
        $idusuario= auth()->user()->id;
        $marca->user_id=$idusuario;
        $marca->observacion = $observacion;
        $marca->idpais = $aDatos['idpais'];
        $marca->save();
        return $marca;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(validarMarcaRequest $request, $id)
    {
        $marca = Marcas::find($id);
        $validatedData = $request->validated();
        return $this->guardarRegistro($validatedData, $marca);        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $marca = Marcas::find($id);
        $marca->delete();
    }
}
