<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

use App\Marcas;

class MarcasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            $id=$request->input('id');
            $datos=array();
            if(!$id){
                $aWhere=array();
                $nombre=$request->input('nombre');
                if($nombre){
                    $aWhere[]=array('marcas.nombre', 'like', $nombre);
                }
                $idpais=$request->input('idpais');
                if($idpais){
                    $aWhere[]=array('marcas.idpais', '=', $idpais);
                }
                var_dump($nombre, $idpais);
                $datos=DB::table('marcas')
                ->join('paises', 'marcas.idpais', '=', 'paises.idpais')
                ->select('marcas.idmarca as id', 'marcas.nombre', 'paises.nombre as pais', 'marcas.observacion')
                ->where($aWhere) 
                ->get();
            }else{
                $datos=DB::table('marcas')
                ->select('marcas.*')
                ->where('idmarca', '=', $id)
                ->get();       
            }
            return $datos;            
        }else{
            return view('home');
        }
    }

    public function buscarPaises(){
        return DB::table('paises')
            ->select('paises.idpais as value', 'paises.nombre as text')
            ->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $marca = new Marcas();
        return $this->guardarRegistro($request, $marca);        
    }

    public function guardarRegistro($request, $marca){
        $marca->nombre = $request->nombre;
        $observacion="";
        if($request->observacion){
            $observacion=$request->observacion;
        }
        $idusuario= $request->user()->id;
        $marca->user_id=$idusuario;
        $marca->observacion = $observacion;
        $marca->idpais = $request->idpais;
        $marca->save();
        return $marca;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $marca = Marcas::find($id);
        return $this->guardarRegistro($request, $marca);        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $marca = Marcas::find($id);
        $marca->delete();
    }
}
