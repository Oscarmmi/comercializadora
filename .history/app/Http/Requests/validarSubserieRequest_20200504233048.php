<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class validarSubserieRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'idsubserie' => 'numeric',
            'nombre' => 'required|max:255',
            'observacion' => 'max:500',
            'idserie' => 'required|numeric'
       ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'idsubserie.numeric' => '- La subserie ingresada no es valida',
            'nombre.required' => '- El nombre de la serie es requerido',
            'nombre.max:255' => '- El nombre de la serie no puede tener mas de 255 caracteres',
            'idserie.required'  => '- Debe selecccionar una marca', 
            'idmarca.numeric'  => '- La marca seleccionado no es valido'
        ];
    }
}
