<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class validarSubserieRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'idsubserie' => 'numeric',
            'nombre' => 'required|max:255',
            'observacion' => 'max:500',
            'idserie' => 'required|numeric'
       ];
    }
}
