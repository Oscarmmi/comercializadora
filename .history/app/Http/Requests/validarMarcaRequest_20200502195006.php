<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class validarMarcaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return  [
            'nombre' => 'required|max:255',
            'observacion' => 'alpha_num',
            'idpais' => 'required|alpha_num'
       ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'nombre.required' => '- El nombre de la marca es requerido',
            'idpais.required'  => '- Debe selecccionar un pais'
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'nombre' => 'trim|capitalize|escape',
            'observacion' => 'trim|capitalize|escape'
        ];
    }

}
