<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class validarSerieRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'idserie' => 'numeric',
            'nombre' => 'required|max:255',
            'observacion' => 'max:500',
            'idmarca' => 'required|numeric'
       ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'idserie.numeric' => '- La serie ingresada no es valida',
            'nombre.required' => '- El nombre de la serie es requerido',
            'nombre.max:255' => '- El nombre de la serie no puede tener mas de 255 caracteres',
            'idmarca.required'  => '- Debe selecccionar una marca', 
            'idpais.numeric'  => '- El pais seleccionado no es valido'
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'nombre' => 'trim|capitalize|escape',
            'observacion' => 'trim|capitalize|escape'
        ];
    }
}
