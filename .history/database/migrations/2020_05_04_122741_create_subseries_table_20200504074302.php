<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubseriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subseries', function (Blueprint $table) {
            $table->bigIncrements('idsubserie');
            $table->string('nombre');
            $table->text('observacion');
            $table->bigInteger('idserie')->unsigned();
            $table->bigInteger('user_id')->unsigned();
            $table->foreign('idmarca')->references('idmarca')->on('marcas');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subseries');
    }
}
