require('./bootstrap');

import Vue from 'vue'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'



window.Vue = require('vue');

// Install BootstrapVue
Vue.use(BootstrapVue);

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('tabla-paginada', require('./components/tablaPaginadaComponent.vue').default);

const app = new Vue({
    el: '#app',
});
