require('./bootstrap');

import Vue from 'vue'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

import fontawesome from '@fortawesome/fontawesome';
import FontAwesomeIcon from '@fortawesome/vue-fontawesome';


window.Vue = require('vue');

// Install BootstrapVue
Vue.use(BootstrapVue);

Vue.component('inicio', require('./components/InicioComponent.vue').default);
Vue.component('marcas', require('./components/MarcasComponent.vue').default);
Vue.component('tabla-paginada', require('./components/TablaPaginadaComponent.vue').default);
Vue.component('font-awesome-icon', FontAwesomeIcon);

const app = new Vue({
    el: '#app',
});
