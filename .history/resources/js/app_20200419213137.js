require('./bootstrap');

import Vue from 'vue';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';
import fontawesome from '@fortawesome/fontawesome';
import FontAwesomeIcon from '@fortawesome/vue-fontawesome';
import fas from '@fortawesome/fontawesome-free-solid';
import fab from '@fortawesome/fontawesome-free-brands';

import '@fortawesome/fontawesome/styles.css';
import fontawesome from '@fortawesome/fontawesome';
import { faEdit } from '@fortawesome/fontawesome-free-solid';

fontawesome.config = {
  autoAddCss: false,
};

fontawesome.library.add(faEdit);


window.Vue = require('vue');

// Install BootstrapVue
Vue.use(BootstrapVue);

Vue.component('font-awesome-icon', FontAwesomeIcon);
fontawesome.library.add(fas, fab);
Vue.component('inicio', require('./components/InicioComponent.vue').default);
Vue.component('marcas', require('./components/MarcasComponent.vue').default);
Vue.component('tabla-paginada', require('./components/TablaPaginadaComponent.vue').default);


const app = new Vue({
    el: '#app',
});
