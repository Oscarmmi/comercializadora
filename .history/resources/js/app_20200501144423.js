require('./bootstrap');

import Vue from 'vue';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { mdbBtn } from 'mdbvue';
import Vuex from 'vuex';


library.add(fas, fab);

Vue.config.productionTip = false;


window.Vue = require('vue');
Vue.use(Vuex);

// Install BootstrapVue
Vue.use(BootstrapVue);
Vue.use(BootstrapVue);

Vue.component('icon', FontAwesomeIcon);
Vue.component('inicio', require('./components/InicioComponent.vue').default);
Vue.component('marcas', require('./components/MarcasComponent.vue').default);
Vue.component('tabla-paginada', require('./components/TablaPaginadaComponent.vue').default);

const app = new Vue({
    el: '#app'
});
