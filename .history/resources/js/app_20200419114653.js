require('./bootstrap');

import Vue from 'vue'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'



window.Vue = require('vue');

// Install BootstrapVue
Vue.use(BootstrapVue);

Vue.component('marcas', require('./components/MarcasComponent.vue').default);
Vue.component('tabla-paginada', require('./components/TablaPaginadaComponent.vue').default);

const app = new Vue({
    el: '#app',
});
