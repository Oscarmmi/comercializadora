require('./bootstrap');

window.Vue = require('vue');

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('tabla-paginada', require('./components/tablaPaginadaComponent.vue').default);

const app = new Vue({
    el: '#app',
});
