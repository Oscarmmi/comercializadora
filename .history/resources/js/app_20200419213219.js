require('./bootstrap');

import Vue from 'vue';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';
import fontawesome from '@fortawesome/fontawesome';
import FontAwesomeIcon from '@fortawesome/vue-fontawesome';
import falight from '@fortawesome/fontawesome-pro-light';
import fasolid from '@fortawesome/fontawesome-pro-solid';

fontawesome.library.add(falight, fasolid);

 Vue.component(FontAwesomeIcon.name, FontAwesomeIcon);



window.Vue = require('vue');

// Install BootstrapVue
Vue.use(BootstrapVue);

Vue.component('font-awesome-icon', FontAwesomeIcon);
fontawesome.library.add(faEdit);
fontawesome.library.add(fas, fab);

Vue.component('inicio', require('./components/InicioComponent.vue').default);
Vue.component('marcas', require('./components/MarcasComponent.vue').default);
Vue.component('tabla-paginada', require('./components/TablaPaginadaComponent.vue').default);


const app = new Vue({
    el: '#app',
});
